package org.mustafababil.ab2015intent;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button changeToSecActBut = (Button) findViewById(R.id.button);
        changeToSecActBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder askYou = new AlertDialog.Builder(MainActivity.this);
                askYou.setTitle(getString(R.string.sureMessage));
                askYou.setMessage("Would you like to open 2nd Activity?");

                //Prepare positive respond
                askYou.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //start 2nd activity here
                        Intent intent = new Intent(MainActivity.this, SecondActivity.class);
                        intent.putExtra("userID", 21000490);
                        intent.putExtra("address", "Eskisehir");
                        startActivity(intent);
                    }
                });

                //Prepare negative respond
                askYou.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(MainActivity.this, "Process Canceled.", Toast.LENGTH_SHORT).show();
                    }
                });

                //Dont forget to show the Dialog after built
                askYou.show();

            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
