package org.mustafababil.ab2015intent;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;


public class SecondActivity extends ActionBarActivity {

    private int userID;
    private String address;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        //if there is no data associated with 'userID' key, then set variable to -1
        userID = getIntent().getIntExtra("userID", -1);
        address = getIntent().getStringExtra("address");

        Toast.makeText(this, "Intent Content: " + userID + " and " + address, Toast.LENGTH_LONG).show();


        Button shareBut = (Button) findViewById(R.id.shareBut);
        shareBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareInt = new Intent(Intent.ACTION_SEND);
                shareInt.putExtra(Intent.EXTRA_TEXT, userID + " " + address);
                shareInt.setType("text/plain");
                startActivity(shareInt);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_second, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
