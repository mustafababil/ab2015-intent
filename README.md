activity design view’dayken associate with activity yaparak eslestirebiliyorsun.

andorid:gravity=“center” icindeki nesneleri hizaliyor.

android:layout_gravity=“center” ??

match_parent: bir ust elementin genisligi kadar yer kapla
wrap_content: sigdirabildigin kadar kucult

Explicit Intent:
intent olustururken new Intent() icersine getApplicationContext yazarsan app ayakta oldugu surece main activity de RAM’de yer kaplayacak.
Intent intent = new Intent(MainActivity.this, SecondActivity.class);

putExtra ile veri tasiyabiliyorsun.
iki tane param aliyor, birincisi degiskenin ismi, ikincisi yollanacak veri.
intent.putExtra(“variable_name”, 2015);

yeni acilan Activity’de intent’e yuklenen verileri cekmek icin:
getIntent().getIntExtra(“variable_name”, -1) ile 2015’i cekmis oluyor.
eger Intent’e “variable_name” adinda bir veri yuklenmemisse ikinci parametre olan -1’i aliyor.


Implicit Intent:
baska bir uygulamaya veri yollama
Intent shareInt = new Intent(Intent.ACTION_SEND); //data paylasma intent olusturuyor ACTION_SEND ile sisteme soyluyor gibi dusun.
shareInt.putExtra(Intent.EXTRA_TEXT, userID + " " + address);
EXTRA_TEXT ile variable_name  vermis olduk ama global bir isim, diger uygulamalar da bu vaiable_name ile cekecek kolayca daha onceden karar verilmis oldugu icin.


Alert Dialog:
JS’deki confirm gibi bir sey, evet hayir diye kullanciya soruyor.
title ve text giriyoruz.
daha sonra positif ve negatif buttonlari ve bunlara tiklandigindaki eventleri yaziyoruz.


Strings.xml’e sag tiklayip Open Translations Editor ile yeni dil cevirileri ekleyebiliyoruz.
